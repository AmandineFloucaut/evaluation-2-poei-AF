package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    private RawgDatabaseClient rawgDatabaseClient;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository, RawgDatabaseClient rawgDatabaseClient) {
        this.videoGamesRepository = videoGamesRepository;
        this.rawgDatabaseClient = rawgDatabaseClient;
    }

    @PostConstruct
    private void postConstruct(){
        this.addVideoGame("Mario");
        this.addVideoGame("Tetris");
    }

    public List<VideoGame> ownedVideoGames() {
        return this.videoGamesRepository.getAll();
    }

    public VideoGame getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id)
                .orElseThrow(() -> new IllegalArgumentException("This video game does not exist")) ;
    }

    public VideoGame addVideoGame(String name) {

        VideoGame newGame = rawgDatabaseClient.getVideoGameFromName(name);

        videoGamesRepository.save(newGame);

        return newGame;
    }

    public void deleteVideoGame(VideoGame videoGame){
        this.videoGamesRepository.delete(videoGame);
    }

    public void indicateAfinishedGame(Long id){
        VideoGame currVideoGame = this.videoGamesRepository.get(id)
                .orElseThrow(() -> new IllegalArgumentException("This video game does not exist"));
        currVideoGame.setFinished(Boolean.TRUE);
    }
}
