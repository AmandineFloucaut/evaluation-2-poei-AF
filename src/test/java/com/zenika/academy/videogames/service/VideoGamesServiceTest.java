package com.zenika.academy.videogames.service;


import com.zenika.academy.videogames.repository.VideoGamesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class VideoGamesServiceTest {

    @Autowired
    VideoGamesRepository videoGamesRepository;

    @Autowired
    VideoGamesService videoGamesService;

    @Test
    void testMethodAddGame(){
        // Add new game in repo
        videoGamesService.addVideoGame("Mario-kart");

        assertEquals(1,videoGamesRepository.getVideoGamesById().size() )  ;
    }

    @Test
    void testMethodOwnedVideoGames(){
        assertEquals(videoGamesRepository.getVideoGamesById().values().stream().toList(), this.videoGamesService.ownedVideoGames());
    }
}
